<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

# aws-test-project

Build Laravel Pipeline using Gitlab CICD & Docker, AWS ECR, EKS
Building full Pipeline in GitLab CI-CD for deploying a dockerized Laravel application on AWS EKS

# Infra Laravel Deployment

Laravel Blog webapp

---

## Available Tags

| Containers  |
|-------------|
| Container `kavitha271/laravel-aws-eks` [latest](docs/README-Laravel.md) 

---

## Prerequirements

- [gitlab](https://gitlab.com/)
- [AWS EKS](https://console.aws.amazon.com/eks/home#/clusters)
- [AWS EC2](https://aws.amazon.com/console/)
- [gitlab runner setup](https://docs.gitlab.com/runner/register/)
- [Docker](https://www.docker.com)
- [Docker-Compose](https://github.com/docker/compose/releases)
- [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html)


### **Docs**

Index Documentation, see [here](docs/README.md) for detail

### **compose**

Docker Compose for Build Image, eg:

- PHP (Laravel App)
- Mysql  
- Phpmyadmin
- _etc_


## How-to-Use

- Clone this repository

  ```
  git clone https://gitlab.com/thalluri.kavitha/aws-test-project.git
  ```

- create Dockerfile

  ```
  Dockerfile with PHP base image,composer,mysql
  ```

- Push Docker image to Docker HUB

  ```
  docker push $DOCKER_REGITRY/$DOCKER_IMAGE:TAG
  ```
- AWS CLI install,AWS configure,EKS cluster creation

  ```
  aws configure
  ECR registry push
  EKS CLUSTER creation 
  ```

- Gitlab runner register in EC2
  ```
  register gitlab
  create CICD pipeline in gitlab
  add variables (Docker registry,aws default region,aws     access key,secret key etc) in CICD settings
  ```

## Deploy in EKS

- Deploy laravevel application in EKS using CI/CD pipeline 
 
- Create Namespace `larave-app`
  ```
  kubectl create namespace laravel-app
  ```
- Deploy in  EKS cluster
  ```
  make manifest files for deployment and service
  ```
  
## Tested Environment

### Versioning

- Docker version

  ```
  docker -v
  ---
  Docker version 20.10.17-rd, build c2e4e01

  docker version
  
  
- Docker-Compose version

  ```
  docker-compose -v
  ---
  Docker Compose version v2.11.1
  ```

- AWS Cli

  ```
  aws --version
  ---
- Gitlab
  
  ```
  gitlab-runner status

  ```
- Laravel Blog app

   ![laravel](./images/laravelapp.png)
     
